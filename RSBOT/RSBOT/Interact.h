#include <Windows.h>
#include <stdio.h>

#ifndef INTERACT_H
#define INTERACT_H

void LeftClick(POINT p);

void RightClick(POINT p);

void MovCurs(POINT p);

COLORREF getColorAtCursor(void);

#endif