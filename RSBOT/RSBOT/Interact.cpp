#include "Interact.h"

/*************************************************************************
*	Function: LeftClick
*	Author: Christian Dumas
*	Date created: 7/23/2015
*	Date last edited: 7/23/2015
*	Description: Left clicks at the given POINT
*************************************************************************/

void LeftClick(POINT p)
{
	SetCursorPos(p.x, p.y);
	mouse_event(MOUSEEVENTF_LEFTDOWN, p.x, p.y, 0, 0);
	Sleep(10);
	mouse_event(MOUSEEVENTF_LEFTUP, p.x, p.y, 0, 0);
}

/*************************************************************************
*	Function: LeftClick
*	Author: Christian Dumas
*	Date created: 7/23/2015
*	Date last edited: 7/23/2015
*	Description: Right clicks at the given POINT
*************************************************************************/

void RightClick(POINT p)
{
	SetCursorPos(p.x, p.y);
	mouse_event(MOUSEEVENTF_RIGHTDOWN, p.x, p.y, 0, 0);
	Sleep(10);
	mouse_event(MOUSEEVENTF_RIGHTUP, p.x, p.y, 0, 0);
}

/*************************************************************************
*	Function: MovCurs
*	Author: Christian Dumas
*	Date created: 7/23/2015
*	Date last edited: 7/23/2015
*	Description: Moves the cursor to the given POINT
*************************************************************************/

void MovCurs(POINT p)
{
	SetCursorPos(p.x, p.y);
}

/*************************************************************************
*	Function: GetColor
*	Author: RosettaCode, Christian Dumas
*	Date created: N/A
*	Date last edited: 7/23/2015
*	Description: Obtains the color at the given POINT
*************************************************************************/

COLORREF GetColor(POINT p) {
	COLORREF color;
	HDC hDC;

	/* Get the device context for the screen */
	if (hDC = GetDC(NULL))
		return CLR_INVALID;

	/* Retrieve the color at that position */
	color = GetPixel(hDC, p.x, p.y);

	/* Release the device context again */
	ReleaseDC(GetDesktopWindow(), hDC);

	return color;
}